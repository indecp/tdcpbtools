# tdcpbtools
A collection of tools to manipulate DCPS

## install
```
cd $HOME/dev
git clone git@gitlab.com:indecp/tdcpbtools.git
cd tdcpbtools
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```



## Quick DCP verification
Only verify DCP file sizes. Compare size of files in DCP folder with file given in PKL file.
usage:
```
tdcpb-checkdcp-short /path/to/DCP
```

## Search Dcp on torrent client

Show torrent client information of a DCP

~~~
bin/tdcpb-search-dcp -p <password> -l <login> <host-or-IP> <DCP name>
~~~

