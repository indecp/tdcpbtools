#!/usr/bin/python
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4

import os
import logging
import sys
import json
import socket
import pprint
from time import sleep
from datetime import datetime
from datetime import timedelta

from transmissionrpc import HTTPHandlerError, TransmissionError
from transmissionrpc import Client as TClient, Torrent
from . import logger


from . import TdcpbException
from . import logger

TRANSMISSION_CLIENT = "transmission"
DELUGE_CLIENT       = "deluge"

CLIENT_TYPE = [
        TRANSMISSION_CLIENT,
        DELUGE_CLIENT]

class BTCexception(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class BitTorrentClient(object):
    def __init__(self,client_type = TRANSMISSION_CLIENT):
        self.client_type= client_type
    def connect(self, address, port, user, password):
        pass
    def get_torrents(self):
        pass
    def add_torrent(self, torrent_path):
        pass
    def remove(self, torrent_name):
        pass

   
class TransmissionClient(BitTorrentClient):
    DELETE_TIMEOUT = 240

    def __init__(self):
        BitTorrentClient.__init__(self, TRANSMISSION_CLIENT)
        self.dict_client={}

    def connect(self, address, port, user, password):
        try :
           self.client =TClient( address, port, user, password)
        except TransmissionError as err:
            raise TdcpbException("{} TransmissionError: {}".format(address,err))
        except socket.timeout as err:
            raise TdcpbException("{} Socket error: {}".format( address,  err))
        except:
            raise TdcpbException("{} Unexpected error: {}".\
                                 format( address,sys.exc_info()[0]))
        else:
            self.dict_client['name']= address

    def dump_torrent_data(self, torrent):
        _dict = {
            u'name'         : torrent.name,
            u'hash'         : torrent.hashString,
            u'progress'     : torrent.progress,
            u'status'       : torrent.status,
            u'date_active'  : torrent.date_active,
            u'date_added'   : torrent.date_added,
            u'date_started' : torrent.date_started,
            u'date_done'    : torrent.date_done,
            u'eta'          : -1,
            u'error'        : torrent.error,
            u'errorString'  : torrent.errorString[:350],
            u'tid'          : torrent.id,

        }
        #TODO: Warning eta can return exception ValuerError
        try:
            _dict[ u'eta']          = timedelta.total_seconds(torrent.eta)
        except ValueError:
            pass
        if _dict[u'error'] == 3:
            _dict[u'status'] = u'error_torrentclient'
        if _dict[u'error'] in [1,2]:
            _dict[u'status'] = u'error_torrenttracker'
        return _dict

    def get_torrent_by_name(self, name):
        tr_torrents = self.client.get_torrents()
        for _torrent in tr_torrents:
            if _torrent.name == name :
                return self.dump_torrent_data(_torrent)
        return None


    def get_torrents(self):
        tr_torrents = self.client.get_torrents()
        self.dict_client['torrents']= []
        for _torrent in tr_torrents:
            self.dict_client['torrents'].append(self.dump_torrent_data(_torrent))
        return self.dict_client

    def add_torrent(self, torrent_path):
        pass
    def remove(self, torrent_hash, delete_data = False) :
        if delete_data:
            # increase temeout wgne deleting data
            self.client.remove_torrent(torrent_hash,
                                     delete_data = delete_data,
                                     timeout = self.DELETE_TIMEOUT )
        else:
            self.client.remove_torrent(torrent_hash)

    def verify(self, torrent_hash):
        self.client.verify_torrent(torrent_hash)

    def free_space(self):
        session = self.client.get_session()
        return (1.0*session.download_dir_free_space)/2**30

def search_dcp(dcp,host,user,password):
    _tc = TransmissionClient()
    try:
        _tc.connect(
                address =host,
                port = 9091,
                user = user,
                password = password)
    except TdcpbException as err:
        logger.error('{}'.format(err))
        raise
    res = _tc.get_torrent_by_name(dcp)
    return res




def main(argv):
    pass
