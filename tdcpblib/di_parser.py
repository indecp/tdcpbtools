#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4
#
#
# Copyright Nicolas Bertrand (nico@isf.cc), 2013
#
# This file is part of DcpIngest.
#
#    DcpIngest is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Luciole is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Luciole.  If not, see <http://www.gnu.org/licenses/>.
#
#
#
# ref document :
#   SMPTE 429-9-2007 D-Cinema Packaging — Asset Mapping and File Segmentation
#   SMPTE 429-8-2007 D-Cinema Packaging — Packing List

import sys
import os.path
import os
from lxml import etree
import pprint
import json

import base64
import hashlib
import binascii
from functools import partial
from urllib.parse import urlparse

from .common import TdcpbException
from . import logger

def list_all_files(p_path):
    fileList = []
    fileSize = 0
    folderCount = 0
    for root, subFolders, files in os.walk(p_path):
        folderCount += len(subFolders)
        # add empty folder to fileList
        for _folder in subFolders:
            _f = os.path.join(root,_folder)
            if not os.listdir(_f):
                fileList.append(_f)
        # add files
        for file in files:
            f = os.path.join(root,file)
            fileSize = fileSize + os.path.getsize(f)
            fileList.append(f)
    logger.debug("Total Size is {0} bytes".format(fileSize))
    logger.debug("Total Files: {} ".format(len(fileList)))
    logger.debug("Total Folders: {}".format(folderCount))
    # return relative path
    fileList = [ _w.split(p_path)[1] for _w in fileList]
    # remove leading "/"
    fileList = [ _w[1:] for _w in fileList]
    return sorted(fileList)

def URItoPath(p_path):
    _parsed = urlparse(p_path.strip())
    _abs_path = ''.join([_parsed.netloc, _parsed.path])
    if _abs_path.startswith("/"):
        #remove leading "/"
        _abs_path = _abs_path[1:]
    return _abs_path

class DiError(TdcpbException):
    def __init__(self,value):
        self.value= value
    def __str__(self):
        return repr(self.value)


class XmlParser(object):
    def __init__(self, p_xml_path):
        self.p_xml_path = p_xml_path
        self.ns = {}
        try:
            self.tree = etree.parse(self.p_xml_path)
        except IOError as msg:
            _msg="Parser Error in file {}: {}".format(self.p_xml_path, msg)
            raise DiError(_msg)
        except etree.XMLSyntaxError as _msg:
            _err = "File {}: {}".format(self.p_xml_path, _msg)
            raise DiError(_err)
        self.root = self.tree.getroot()

    def _GetNamespaces(self, prefix="prefix"):
        """ Get Namespaces """
        _ns = self.tree.getroot().tag[1:].split("}")[0]
        self.ns = {prefix: _ns}
        logger.debug("Namespace is {}".format(self.ns))


class AssetmapParser(XmlParser):
    tags=['Id','AnnotationText','IssueDate','Issuer','Creator',
         'VolumeCount']

    def __init__(self, p_xml_path):
        super().__init__(p_xml_path)
        self._GetNamespaces('am')
        self.data = {}
        self.assets = {}
        self._Verify()

    def Dump(self):
        if self.data is None:
            return
        return json.dumps(self.data, sort_keys=True, indent=2)
    def DumpToDict(self):
        if self.data is None:
            return
        return self.data




    def GetAllAssets(self) :
        _assets = self.tree.findall('./am:AssetList/am:Asset', namespaces = self.ns)
        for _asset in _assets:
            _id = _asset.find('am:Id', namespaces = self.ns)
            _path = _asset.find("./am:ChunkList/am:Chunk/am:Path", namespaces = self.ns)
            self.assets[_id.text] = URItoPath(_path.text)
        return self.assets

    def GetPkls(self) :
        self.pkls = []
        _assets = self.tree.findall('./am:AssetList/am:Asset', namespaces = self.ns)
        for _asset in _assets:
            _pkl = _asset.find('am:PackingList', namespaces = self.ns)
            if (_pkl is not None):
                if (_pkl.text != "false"):
                    self.pkls.append(_asset.find('am:Id', namespaces = self.ns).text)
        return self.pkls


    def _FindOne(self, p_tag, p_elem = None) :

        if p_elem is not None :
            _elem = p_elem.findall(p_tag, namespaces = self.ns)
        else :
            _elem = self.tree.findall(p_tag, namespaces = self.ns)
        if len( _elem) != 1:
            _emsg = "Find tag {} {} times. Exepcted only one.".format(p_tag,
                                                                      len(_elem))
            raise DiError(_emsg)
        return _elem[0]

    def _Verify(self):
        for _tag in self.root.findall('am:VolumeCount', namespaces = self.ns):
            _count = int(_tag.text)
            if _count > 1:
                _emsg = "The tool does not support more the one VOLINDEX"
                raise DiError(_emsg)
        for _tag in self.tags:
            _res =  self.tree.find('./am:{}'.format(_tag), self.ns)
            if _res is not None:
                self.data[_tag] = _res.text
        self.data['AssetList'] = []

        _tag = './am:AssetList'
        _asset_list = self._FindOne(_tag)
        _tag = './am:Asset'
        _assets =_asset_list.findall(_tag, namespaces = self.ns)
        if not _assets:
            _emsg = "No Asset found. Invalid AssetMap"
            raise DiError(_emsg)

        _pkl_count = 0
        for _asset in _assets:
            data = {}
            # check Id presence
            _tag = 'Id'
            data[_tag] = self._FindOne('./am:{}'.format(_tag), _asset).text
            _tag = './am:PackingList'
            _pkl =_asset.find(_tag, namespaces = self.ns)
            data['ispkl'] = False
            if (_pkl is not None):
                if (_pkl.text != "false"):
                    _pkl_count += 1
                    data['ispkl'] = True
            _tag = './am:ChunkList'
            _chunk_list = self._FindOne(_tag, _asset)
            _tag = './am:Chunk'
            _chunk = _chunk_list.findall(_tag, namespaces = self.ns)
            if not _chunk :
                _emsg =" No Chunk found"
                raise DiError(_emsg)
            if len(_chunk) > 1 :
                _emsg = "Tool doesn't handle segmentation (i.e. several vol index)"
                raise DiError(_emsg)
            _tag = 'Path'
            data[_tag] = self._FindOne('./am:{}'.format(_tag), _chunk[0]).text
            self.data['AssetList'].append(data)
        if _pkl_count == 0 :
            _emsg = "No PKL found."
            raise DiError(_emsg)


class PklParser(AssetmapParser):

    def __init__(self,p_xml_path, pkl_urn_id,  dcp_base_path, am_assets= None):
        self.data={}
        self.dcp_base_path = dcp_base_path
        self.am_assets = am_assets
        self.pkl_urn_id = pkl_urn_id
        AssetmapParser.__init__(self,p_xml_path)

    def _GetNamespaces(self, prefix='pkl'):
        """ Get Namespaces """
        _ns = self.tree.getroot().tag[1:].split("}")[0]
        self.ns = {'pkl': _ns}

    def GetCpls(self):
        _assets = self.tree.findall('./pkl:AssetList/pkl:Asset', namespaces = self.ns)
        cpls = {}
        for _asset in _assets:
            _asset_dict={}
            _tag = 'Type'
            _elem = _asset.find('pkl:{}'.format(_tag), namespaces = self.ns)
            if 'CPL' in  _elem.text:
                _tag = 'Id'
                _id = _asset.find('pkl:{}'.format(_tag), namespaces = self.ns)

                _tag = 'Hash'
                _elem = _asset.find('pkl:{}'.format(_tag), namespaces = self.ns)
                _asset_dict[_tag] = _elem.text

                _tag = 'Size'
                _elem = _asset.find('pkl:{}'.format(_tag), namespaces = self.ns)
                _asset_dict[_tag] = _elem.text

                _tag = 'Type'
                _elem = _asset.find('pkl:{}'.format(_tag), namespaces = self.ns)
                _asset_dict[_tag] = _elem.text

                # parse optional tags
                _tag = 'OriginalFileName'
                _elem = _asset.find('pkl:{}'.format(_tag), namespaces = self.ns)
                if _elem is not None:
                    _asset_dict[_tag] = _elem.text

                _tag = 'AnnotationText'
                _elem = _asset.find('pkl:{}'.format(_tag), namespaces = self.ns)
                if _elem is not None:
                    _asset_dict[_tag] = _elem.text
                cpls[_id.text] = _asset_dict
            elif 'text/xml' in _elem.text:
                _tag = 'Id'
                _id = _asset.find('pkl:{}'.format(_tag), namespaces = self.ns)
                _path = os.path.join(self.dcp_base_path, self.am_assets[_id.text])
                try:
                    _xml_cpl=XmlParser(_path)
                except DiError as err:
                    raise err
        return cpls

    def isCpl(self, elem,  urn_id):
        if 'text/xml' in elem :
            _path = os.path.join(self.dcp_base_path, self.am_assets[urn_id])
            try:
                _xml_cpl=XmlParser(_path)
            except DiError as err:
                raise err
            else:
                if 'CompositionPlaylist' in _xml_cpl.root.tag:
                    return True
        return False

    def ParseCpl(self, urn_id):
        _path = os.path.join(self.dcp_base_path, self.am_assets[urn_id])
        logger.debug("Parsing CPL: {}".format(os.path.basename(_path)))
        try:
            _xml_cpl=CplParser(_path, urn_id)
        except DiError as err:
            raise err
        else:
            self.data['CPLS'].append( _xml_cpl.DumpToDict())


    def GetAssets(self):
        self.data['CPLS']=[]
        self.data['AssetList']= []
        _assets = self.tree.findall('./pkl:AssetList/pkl:Asset', namespaces = self.ns)
        for _asset in _assets:
            _asset_dict={}
            # parse mandatory tags
            _tag = 'Id'
            _id = _asset.find('pkl:{}'.format(_tag), namespaces = self.ns)
            _asset_dict[_tag] = _id.text

            _tag = 'Hash'
            _elem = _asset.find('pkl:{}'.format(_tag), namespaces = self.ns)
            _asset_dict[_tag] = _elem.text

            _tag = 'Size'
            _elem = _asset.find('pkl:{}'.format(_tag), namespaces = self.ns)
            _asset_dict[_tag] = _elem.text

            _tag = 'Type'
            _elem = _asset.find('pkl:{}'.format(_tag), namespaces = self.ns)
            _asset_dict[_tag] = _elem.text

            _asset_dict['isCpl'] = False
            if self.isCpl(_elem.text, _id.text):
                _asset_dict['isCpl'] = True
                self.ParseCpl(_id.text)


            # parse optional tags
            _tag = 'OriginalFileName'
            _elem = _asset.find('pkl:{}'.format(_tag), namespaces = self.ns)
            if _elem is not None:
                _asset_dict[_tag] = _elem.text

            _tag = 'AnnotationText'
            _elem = _asset.find('pkl:{}'.format(_tag), namespaces = self.ns)
            if _elem is not None:
                _asset_dict[_tag] = _elem.text
            self.assets[_id.text] = _asset_dict
            self.data['AssetList'].append(_asset_dict)
        return self.assets

    def DumpPkl(self):
        if self.data is None:
            return
        _pkl_str="Packing List data\n"
        _tag = "AnnotationText"
        _pkl_str += "{:<30}: {}\n".format(_tag, self.data[_tag])
        _tag = "Id"
        _pkl_str += "{:<30}: {}\n".format(_tag, self.data[_tag])
        _tag = "IssueDate"
        _pkl_str += "{:<30}: {}\n".format(_tag, self.data[_tag])
        _tag = "Issuer"
        _pkl_str += "{:<30}: {}\n".format(_tag, self.data[_tag])
        _tag = "Creator"
        _pkl_str += "{:<30}: {}\n".format(_tag, self.data[_tag])

    def _Verify(self):
        _tag = 'Id'
        self.data[_tag] = self._FindOne('./pkl:{}'.format(_tag)).text
        if self.data[_tag] != self.pkl_urn_id:
            _msg = "Id of PKL did not match one in AssetMap"
            logger.error("ID in PKL: {} ID of PKL in AssetMap: {}".format(
                self.data[_tag], self.pkl_urn_id))
            raise DiError(_msg)
        _tag = 'IssueDate'
        self.data[_tag] = self._FindOne('./pkl:{}'.format(_tag)).text
        _tag = 'Issuer'
        self.data[_tag] = self._FindOne('./pkl:{}'.format(_tag)).text
        _tag = 'Creator'
        self.data[_tag] = self._FindOne('./pkl:{}'.format(_tag)).text
        _tag = 'AnnotationText'
        self.data[_tag] = self._FindOne('./pkl:{}'.format(_tag)).text


        _tag = 'AssetList'
        self._FindOne('./pkl:{}'.format(_tag))
        self.GetAssets()

class CplParser(AssetmapParser):
    tags=['AnnotationText', 'IssueDate', 'Issuer', 'Creator',
          'ContentTitleText', 'ContentKind']
    def __init__(self,p_xml_path, urn_id):
        self.data={}
        self.urn_id = urn_id
        AssetmapParser.__init__(self,p_xml_path)

    def _GetNamespaces(self, prefix='cpl'):
        """ Get Namespaces """
        _ns = self.tree.getroot().tag[1:].split("}")[0]
        self.ns = {'cpl': _ns}

    def _Verify(self):
        _tag = 'Id'
        self.data[_tag] = self._FindOne('./cpl:{}'.format(_tag)).text
        if self.data[_tag] != self.urn_id:
            _msg = "Id of PKL did not match one in AssetMap"
            logger.error("ID in PKL: {} ID of PKL in AssetMap: {}".format(
                self.data[_tag], self.urn_id))
            raise DiError(_msg)
        for  _tag in self.tags:
            self.data[_tag] = self._FindOne('./cpl:{}'.format(_tag)).text
        self.GetReels()
        if  self.tree.find('//cpl:KeyId', namespaces = self.ns) is not None:
            self.data['Encrypted'] = True
        else:
            self.data['Encrypted'] = False

    def Dump(self):
        if self.data is None:
            return
        return json.dumps(self.data, sort_keys=True, indent=2)

    def GetReels(self):
        self.data['ReelList']= []
        for _reel in self.tree.findall('./cpl:ReelList/cpl:Reel',
                                       namespaces = self.ns):
            _reel_dict={}
            for _elem in list(_reel):
                if isinstance(_elem, etree._Comment) : continue
                if 'AssetList' in _elem.tag:
                    _tag= _elem.tag.split('}')[1]
                    _reel_dict[_tag] = self.GetAssetList(_elem)
                else:
                    _tag= _elem.tag.split('}')[1]
                    _reel_dict[_tag] = _elem.text
            self.data['ReelList'].append(_reel_dict)



    def GetAssetList(self, assets):
        _dict = {}
        for _elem in list(assets):
            _tag= _elem.tag.split('}')[1]
            _dict[_tag] = self.GetAsset(_elem)
        return _dict

    def GetAsset(self, assets):
        _dict = {}
        for _elem in list(assets):
            _tag= _elem.tag.split('}')[1]
            _dict[_tag] = _elem.text
        return _dict

      



class DiParser(object):

    BUF_SIZE = 65536

    def __init__(self, p_dcp_folder):
        if os.path.exists(p_dcp_folder):
            self.p_dcp_folder = p_dcp_folder
        else:
            _emsg = "Not a DCP folder: {}".format(p_dcp_folder)
            raise DiError(_emsg)
        self.volindex = ""
        self.progress = 0.0

    def list_dcp_files(self):
        try:
            self.getAssetmap()
            self._assetmap_xml = AssetmapParser(self.assetmap_path)
        except DiError as msg:
            logger.error(msg)
            return 0
        self.am_assets = self._assetmap_xml.GetAllAssets()
        _dcp_files = list(self.am_assets.values())
        # insert assetmap file
        _dcp_files.append(os.path.basename(self.assetmap_path))
        # insert VOLINDEX as it is not listed in AssetMap
        # TODO Manage case of several VOLINDEX ie with or without .xml
        _dcp_files.append(self.volindex)
        return sorted(_dcp_files)

    def dump(self):
        try:
            self.getAssetmap()
            self._assetmap_xml = AssetmapParser(self.assetmap_path)
        except DiError as msg:
            logger.error(msg)
            raise DiError(msg)
        data = json.loads(self._assetmap_xml.Dump())
        self.am_assets = self._assetmap_xml.GetAllAssets()
        for PKL in self._assetmap_xml.GetPkls():
            _pkl_path= os.path.join(self.p_dcp_folder, self.am_assets[PKL])
            logger.debug("Parsing PKL: {}".format(os.path.basename(_pkl_path)))
            try:
                _pkl_xml = PklParser(_pkl_path, PKL, self.p_dcp_folder, self.am_assets)
            except DiError as msg:
                logger.error(msg)
                return 0
            _msg = "Found : {} ".format(self.am_assets[PKL])
            logger.info(_msg)
            data['PKL'] = json.loads(_pkl_xml.Dump())
#            pprint.pprint(_pkl_xml.GetAssets())
#            data['PKL']['CPLS']= []
#            for CPL in _pkl_xml.GetCpls():
#                _cpl_path= os.path.join(self.p_dcp_folder, self.am_assets[CPL])
##                logger.debug("Parsing CPL: {}".format(os.path.basename(_cpl_path)))
#                try:
#                    _cpl_xml = CplParser(_cpl_path, CPL)
#                except DiError as msg:
#                    logger.error(msg)
#                    return 0
#                _msg = "Found : {} ".format(self.am_assets[CPL])
#                logger.info(_msg)
#                data['PKL']['CPLS'].append(json.loads(_cpl_xml.Dump()))

        return data

    def check_files(self):
        self.unexpected = None
        _nb_assets = 0
        # 1st check presence of dummy VOLINDEX
        if not self.isVolindexPresent():
            _msg = "No VOLINDEX found in DCP folder({}) ".format(
                self.p_dcp_folder)
            logger.error(_msg)
            return 0
        try:
            self.getAssetmap()
            self._assetmap_xml = AssetmapParser(self.assetmap_path)
        except DiError as msg:
            logger.error(msg)
            return 0
        self.am_assets = self._assetmap_xml.GetAllAssets()
        logger.debug("Found {} assets".format(len(self.am_assets)))
        self.pkls = self._assetmap_xml.GetPkls()
        logger.debug("Found {} PKLS".format(len(self.pkls)))
        if len(self.pkls) == 0:
            _msg = "No PKL found. Bad DCP"
            logger.error(_msg)
            return 0
        for _pkl in self.pkls :
            _pkl_urn_id = _pkl
            _pkl_path= os.path.join(self.p_dcp_folder, self.am_assets[_pkl_urn_id])
            logger.debug("Parsing PKL: {}".format(os.path.basename(_pkl_path)))
            try:
                _pkl_xml = PklParser(_pkl_path, _pkl_urn_id, self.p_dcp_folder, self.am_assets)
            except DiError as msg:
                logger.error(msg)
                return 0
            # Valid pkl file increasse asset counter
            _nb_assets +=1
            _msg = "Found : {} ".format(self.am_assets[_pkl_urn_id])
            logger.debug(_msg)

            _pkl_assets = _pkl_xml.GetAssets()
            try:
                _nb_assets += self._ExistsAssets(_pkl_assets)
            except DiError as _msg:
                logger.error(_msg)
                return 0
            if (_nb_assets != len (self.am_assets)):
                _msg = "Invalid number of assets,( {} in AssetMap, {} counted)"\
                    .format(len(self.am_assets), _nb_assets)

        # check presence of uneeded files
        _dcp_files = self.list_dcp_files()
        _dir_files = list_all_files(self.p_dcp_folder)
        if len(_dir_files) > len(_dcp_files):
            # more files in directorry then expected: not an error.
            logger.info("Unexpected files or dir present in DCP folder {}"\
                .format(self.p_dcp_folder))
            self.unexpected = list(set(_dir_files) - set(_dcp_files))
            logger.info("Unexpected files or dir : ")
            for _f in self.unexpected:
                logger.info("   {}".format(_f))
            logger.info("Files in directory = {} Excpected files in DCP = {}".\
                    format(len(_dir_files), len(_dcp_files)))
            return 1
        elif len(_dir_files) < len(_dcp_files):
            _msg="DCP {} contains less file then expected : files in dir = {} expected fils ={} "\
                .format(self.p_dcp_folder, len(_dir_files),  len(_dcp_files) )
            logger.error(_msg)
            return 0
        else :
            logger.debug("files in DCP and files in Assetmap are coherent")

        return _nb_assets

    def check_hash(self):
        _nb_assets = 0
        self.progress = 0.0
        # 1st check presence of dummy VOLINDEX
        if not self.isVolindexPresent():
            _msg = "No VOLINDEX found in DCP folder({}) ".format(
                self.p_dcp_folder)
            logger.error(_msg)
            return "KO"
        try:
            self.getAssetmap()
            self._assetmap_xml = AssetmapParser(self.assetmap_path)
        except DiError as msg:
            logger.error(msg)
            return "KO"
        self.am_assets = self._assetmap_xml.GetAllAssets()
        logger.debug("Found {} assets".format(len(self.am_assets)))
        self.pkls = self._assetmap_xml.GetPkls()
        if len(self.pkls) == 0:
            _msg = "No PKL found. Bad DCP"
            logger.error(_msg)
            return "KO"
        for _pkl in self.pkls :
            _pkl_urn_id = _pkl
            _pkl_path= os.path.join(self.p_dcp_folder, self.am_assets[_pkl_urn_id])
            logger.debug("Parsing PKL: {}".format(os.path.basename(_pkl_path)))
            try:
                _pkl_xml = PklParser(_pkl_path, _pkl_urn_id, self.p_dcp_folder, self.am_assets)
            except DiError as msg:
                logger.error(msg)
                return "KO"
            # Valid pkl file increasse asset counter
            _nb_assets +=1
            _msg = "Found : {} ".format(self.am_assets[_pkl_urn_id])
            logger.debug(_msg)

            _pkl_assets = _pkl_xml.GetAssets()
            try:
                self._ExistsAssets(_pkl_assets)
                sum_fsize, self.total_iterations = self._VerifyHashDurationEstimation()
                self._VerifyHash()
            except DiError as _msg:
                logger.error(_msg)
                return "KO"
        return "OK"


    def getAssetmap(self) :
        _assetmap = os.path.join(self.p_dcp_folder, "ASSETMAP")
        if os.path.isfile(_assetmap):
            logger.debug("The DCP is in interop format")
            self.assetmap_path = _assetmap
        else:
            _assetmap = os.path.join(self.p_dcp_folder, "ASSETMAP.xml")
            if os.path.isfile(_assetmap):
                logger.debug("The DCP is in SMPTE format")
                self.assetmap_path = _assetmap
            else:
                _emsg="No ASSETMAP file found"
                raise DiError(_emsg)
        return

    def isAssetmap(self, p_file):
        if (p_file == "ASSETMAP") or (p_file == "ASSETMAP.xml"):
            return True
        return False

    def isVolindexPresent(self):
        if os.path.exists(os.path.join(self.p_dcp_folder, "VOLINDEX")):
            self.volindex = "VOLINDEX"
            return True
        if os.path.exists(os.path.join(self.p_dcp_folder, "VOLINDEX.xml")) :
            self.volindex = "VOLINDEX.xml"
            return True
        return False

    def _ExistsAssets(self, p_pkl_assets):
        self.assets = {}
        for _k,_v in p_pkl_assets.items():
            if _k in self.am_assets:
                _asset =_v
                _path = os.path.join(self.p_dcp_folder, self.am_assets[_k])
                if os.path.exists(_path):
                    _asset['Path'] = _path
                    _msg = "Found : {} ".format(_path[len(self.p_dcp_folder):])
                    logger.debug(_msg)
                else:
                    _msg = "Asset {} not in DCP directory".format(
                        _path[len(self.p_dcp_folder):])
                    raise DiError(_msg)
                if os.stat(_path).st_size != int(_v['Size']) :
                    _msg = "Asset {} has wrong size".format(
                        _path[len(self.p_dcp_folder):])
                    logger.error("stat size = {}, Size in PKL = {}".format(
                        os.stat(_path).st_size, _v['Size']))
                    raise DiError(_msg)
                self.assets[_k] = _asset
            else:
                _msg = "Asset with id {} not found in assetmap".format(_k)
                raise DiError(_msg)
        return len(self.assets)


    def _VerifyHashDurationEstimation(self):
        sum_fsize = 0
        iterations= 0
        for _k, _v in self.assets.items():
            sum_fsize += int(_v['Size'])
            iterations += (int(int(_v['Size'])/self.BUF_SIZE) +1)
        return (sum_fsize, iterations)

    def _VerifyHash(self ):
        self.iteration_count = 0
        for _k, _v in self.assets.items():
            expected =(base64.b64decode(_v['Hash']))
            _msg = "Checking hash of {} (expected hash = {})".format(_v['Path'], 
                    _v['Hash'])
            logger.debug(_msg)
            _sum = self._HashSum(_v['Path'])
            if _sum == expected:
                _msg = " Hash verification OK ({})".format(_v['Path'])
                logger.debug(_msg)
            else:
                _msg = " Hash verification failed for file {} \n\
CALC SUM = {}\nEXPT SUM = {} ".format(
                    _v['Path'],
                    base64.b64encode(_sum),
                    base64.b64encode(expected))
                logger.error(_msg)
                raise DiError(_msg)

    def _HashSum(self, p_filepath):
        """ check SHA1 of DCP files.
            As defined in SMPTE 429-8-2007 section 6.3"""
        sha1 = hashlib.sha1()
        fsize = os.stat(p_filepath).st_size
        with open(p_filepath, 'rb') as f:
            while True:
                data = f.read(self.BUF_SIZE)
                if not data:
                    break
                sha1.update(data)
                self.iteration_count += 1
                self.progress = (self.iteration_count/self.total_iterations) *100


        return sha1.digest()


if __name__ == "__main__":
    DCP = DiParser(sys.argv[1])
    dcp_files = DCP.list_dcp_files()
    print (dcp_files)
