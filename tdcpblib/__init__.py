from enum import Enum, IntEnum
import threading

from .common import create_logger, TdcpbException
logger = create_logger()
from .torrent_client import search_dcp

CONFIG_FILE="/etc/transmission-daemon/exec-done.json"



class WorkerResult(Enum):
    N_A=0,
    NOT_DONE=1
    ON_PROGRESS=2
    OK=3
    FAIL=4

class IngestAutoStep(IntEnum):
    NOT_DONE=1
    COPYING=2
    VERIFYING=3
    CREATING_TORRENT=4
    DONE =5


class TdcpbLibThread(threading.Thread):
    def __init__(self, *args, **kwargs):
        super.__init__(*args, **kwags)
        self.progess =0.0


    def get_progress():
        pass

