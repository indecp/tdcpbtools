#!/usr/bin/python
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4

from . import logger
import MySQLdb

class Tracker
    def __init__(self, host, user, password, database):
        try:
            logger.debug("Connection to databse in {]".format(host)
            self.db = MySQLdb.connect(host,
                                      user,
                                      password,
                                      database)
            self.cursor = self.db.cursor(MySQLdb.cursors.DictCursor)
        except MySQLdb.Error, e:
            print e.args
            logging.error(' %d: %s' % (e.args[0], e.args[1]))
            sys.exit(1)
        else:
            logger.debug('Connected to DB {}'.format(app.config['XBT_HOST']))

    def _execute_select(self, p_query, p_query_params = None):
        if p_query_params is None :
            _lines = self.cursor.execute(p_query)
        else :
            _lines = self.cursor.execute(p_query, p_query_params)
        _xbt_result =[]
        while True:
            row = self.cursor.fetchone()
            if row == None:
                break
            _xbt = {}
            _xbt['id']         = int(row['id'])
            _xbt['hash']       = binascii.hexlify(row['info_hash'])
            _xbt['event']      = int(row['event'])
            _xbt['downloaded'] = int(row['downloaded'])
            _xbt['left0'] = int(row['left0'])
            _xbt['ts']      = datetime.fromtimestamp(int(row['mtime']))
            _xbt['ipa']        = ipaddr.IPv4Address(row['ipa'])
            _xbt_result.append(_xbt)
        return _xbt_result

     def search_dcp(self, p_hash) :
        _query = "SELECT * FROM xbt_announce_log WHERE event = 1 AND info_hash=%s"
        _query_params(p_hash)
        return self._execute_select(_query, _query_params)


