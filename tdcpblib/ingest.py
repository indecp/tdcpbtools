#
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4
#
# Copyright Nicolas Bertrand (nicolas@indecp.org), 2018
#
# This file is part of tdcpbtools.
#
#    tdcpbtools is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tdcpbtools is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tdcpbtools.  If not, see <http://www.gnu.org/licenses/>.
#
#
#
# Usage:
# TODO

import sys
import stat
import timeit
import argparse
import time
import os
import sys
import queue
import threading
import time
import subprocess as SP
from datetime import datetime

from shutil import copytree, rmtree, ignore_patterns
from shutil import Error as shutilError

#from cp_dcp import copy_dcp
#from CheckFiles import tdcpb_check_files
#from DcpCheckHash import tdcpb_check_hash
#from make_torrent import tdcpb_make_torrent
from .tdcpb_checks import tdcpb_check_short, TdcpbCheckThread
from .common import TdcpbException, get_size, sizeof_fmt
from .torrent_meta import TorrentMeta

from . import logger, WorkerResult, TdcpbLibThread, IngestAutoStep

TRACKER="http://10.10.10.31:2710/announce"
TRACKERS = [
    "http://10.10.10.31:2710/announce",
    "http://10.10.10.103:2710/announce",
]
COMMENT="Created with a slice of lemon pie"



class Copy(TdcpbLibThread):
    def __init__(self, queue, source, destination, ignore_files=None):
        threading.Thread.__init__(self)
        self.queue = queue
        if not os.path.isdir(source):
            _msg = "Source {} is not a directory".format(source)
            raise TdcpbException(_msg)
        self.dcp_name = os.path.basename(source)

        if not os.path.isdir(destination):
            _msg = "Destination {} is not a directory".format(destination)
            raise TdcpbException(_msg)

        self.source = source
        self.source_size= get_size(self.source)
        self.destination = os.path.join(destination, self.dcp_name)
        self.ignore_files = [".*"]
        if ignore_files :
            for _file in ignore_files:
                _list = _file.split("/")
                if _list[0] not in self.ignore_files:
                    self.ignore_files.append(_list[0])

    def run(self):
        if os.path.exists(self.destination):
            try:
                rmtree(self.destination)
            except:
                _msg = "rmtree failed: {}".format( sys.exc_info())
                self.queue.put((WorkerResult.FAIL, _msg ) )
                return

        try:
            # Copy DCP
            ignore = ignore_patterns(*self.ignore_files)
            copytree(self.source , self.destination, ignore= ignore)
        except shutilError as e:
            _msg = "{}".format(e.args[-1][-1][-1])
            _msg = u"Copy KO for source  {} dest {}: {}".\
                format(self.source, self.destination,  _msg)
            logger.error(_msg)
            self.queue.put((WorkerResult.FAIL, _msg ) )
            return
        except OSError as e:
            _msg = "{}".format(e)
            logger.error(u"Copy KO for {}, {}".format(self.source, _msg))
            self.queue.put((WorkerResult.FAIL, _msg ) )
            return
        except:
            _msg= 'Unhandled exception on copy : {}', sys.exc_info()[0]
            logger.error(_msg)
            self.queue.put((WorkerResult.FAIL, _msg ) )
            return

        logger.info(u"Copy Ok for {}".format(self.dcp_name))
        self.queue.put((WorkerResult.OK, "Copy OK") )
        return


    def get_progress(self):
        current = get_size(self.destination)
        self.progress = ( (current*1.0)/self.source_size)*100
        return self.progress



class TorrentCreatorThread (threading.Thread):
    def __init__(self, torrent_path, dcp_path, queue):
        threading.Thread.__init__(self)
        self.torrent_path = torrent_path
        logger.debug("torrent path {}".format(torrent_path))
        self.dcp_path = dcp_path
        self.trackers = TRACKERS
        self.torrent_comment = COMMENT
        self.queue = queue
        self.duration_estimation = None
        self.start_time = timeit.default_timer()

    @property
    def duration_estimation(self):
        return self.__duration_estimation

    @duration_estimation.setter
    def duration_estimation(self, duration_estimation):
        self.__duration_estimation = duration_estimation


    def get_progress(self):
        passed_time = timeit.default_timer() - self.start_time
        self.progress = None
        if self.__duration_estimation :
            self.progress = (passed_time / self.__duration_estimation) * 100
            if self.progress > 100.0 and self.is_alive():
                self.progress=99.9
        return self.progress


    def run(self):
        #_cmd = "/usr/bin/transmission-create -p -t \"{}\" -o \"{}\" -c \"{}\" {}"\
        #    .format(self.announce, self.torrent_path, self.torrent_comment, self.dcp_path)
        _cmd = [
            "/usr/bin/transmission-create",
            "-p",   # private torrent
            "-o",  # output torrent
            self.torrent_path,
            "-c",   # comment
            self.torrent_comment]
        for tracker in self.trackers:
            _cmd = _cmd + ["-t", tracker]

        _cmd.append(self.dcp_path)
        logger.debug("torrent command: {}".format(" ".join(_cmd)))

        _sp = SP.Popen(_cmd, stdout=SP.PIPE, stderr=SP.PIPE)
        _stdout, _stderr = _sp.communicate()
        logger.debug(_stdout)
        logger.debug(_stderr)
        if not _stderr:
            # set torrent readable by all
            os.chmod(self.torrent_path,
                    stat.S_IRUSR |  stat.S_IWUSR | \
                    stat.S_IRGRP |  stat.S_IWGRP | \
                    stat.S_IROTH)
            self.queue.put((WorkerResult.OK, "Torrent creation success" ) )
        else:
            self.queue.put((WorkerResult.FAIL, "Torrent creation failded" ) )

        try:
        	tinfo = TorrentMeta.info(self.torrent_path)
        except:
            logger.error('Unhandled exception', exc_info=sys.exc_info())
        else:
            logger.info("{} size = {} : created on {} hash {}".format(
                self.torrent_path,
                sizeof_fmt(tinfo['size']),
                tinfo['creation_date'],
                tinfo['hash']))
            self.progress =100.0

class IngestThread(threading.Thread):

    def __init__(self, queue, p_source, p_dir_torrent, b_copy_dcp=True,  p_destination =None):
        threading.Thread.__init__(self, name = "Ingest")
        logger.info("Starting DCP ingest: p_source {} p_dir_torrent  {} \
b_copy_dcp {} p_destination {}".format(p_source, p_dir_torrent, b_copy_dcp,
                                          p_destination))

        self.queue = queue
        self.dcp_source_path = p_source
        self.dcp_name = os.path.basename(self.dcp_source_path)
        self.torrent_files_path = p_dir_torrent
        self.copy_dcp = b_copy_dcp
        if  not self.copy_dcp:
            self.destination_path = self.dcp_source_path
        else:
            self.destination_path = p_destination
        self.isDone = False
        self.ingest_step = IngestAutoStep.NOT_DONE
        self.progress = 0.0
        self.worker_state = None
        self.ingest_dcp_state = WorkerResult.NOT_DONE
        self.thread = None
        self.meta={}

    def set_chmod(self, path):
        if os.path.isdir(path):
            os.chmod(path,0o755)
            for root,dirs,files in os.walk(path):
                for d in dirs:
                    os.chmod(os.path.join(root,d),0o755)
                for f in files:
                    os.chmod(os.path.join(root,f),0o644)


    def queue_put(self, *args,**kwargs):
        queue_state= {
            'ingest_dcp_state': self.ingest_dcp_state,
            'ingest_step':self.ingest_step
        }
        for k,v in kwargs.items():
            queue_state[k] = v
        self.queue.put(queue_state)


    def run(self):
        logger.debug("Starting DCP ingest")
        self.ingest_dcp_state = WorkerResult.ON_PROGRESS
        self.meta['ingest_dcp_state'] =  self.ingest_dcp_state

        # 1st verify if DCP is present and well formed
        try:
            unexpected_files = tdcpb_check_short(self.dcp_source_path)
        except TdcpbException as _err:
            self.ingest_dcp_state = WorkerResult.FAIL
            self.ingest_step = IngestAutoStep.DONE
            self.queue_put(exception = sys.exc_info())
            return
        if self.copy_dcp:
            try:
                if  os.path.exists( os.path.join(self.destination_path, self.dcp_name)):
                    self.ingest_dcp_state = WorkerResult.FAIL
                    self.ingest_step = IngestAutoStep.DONE
                    _err = "DCP directory {} already exists in {}. Impossible to copy"\
                        .format(self.dcp_name, self.destination_path)
                    raise TdcpbException(_err)
                self.worker_state = self.copy(unexpected_files)
                if self.worker_state == WorkerResult.FAIL:
                    self.ingest_dcp_state = WorkerResult.FAIL
                    self.ingest_step = IngestAutoStep.DONE
                    raise TdcpbException(self.worker_msg)
            except TdcpbException as _err:
                self.queue_put(exception = sys.exc_info())
                return
        try:
            self.worker_state = self.verify()
            if self.worker_state == WorkerResult.FAIL:
                self.ingest_dcp_state = WorkerResult.FAIL
                self.ingest_step = IngestAutoStep.DONE
                raise TdcpbException(self.worker_msg)
        except TdcpbException as _err:
            self.queue_put(exception = sys.exc_info())
            return

        self.meta['hash_verification_state'] =  self.worker_state
        self.meta['hash_verification_date'] =  datetime.now()

        try:
            self.worker_state = self.create_torrent()
            if self.worker_state == WorkerResult.FAIL:
                self.ingest_dcp_state = WorkerResult.FAIL
                self.ingest_step = IngestAutoStep.DONE
                raise TdcpbException(self.worker_msg)
        except TdcpbException as _err:
            self.queue_put(exception = sys.exc_info())
            return
        print ('destination path', self.destination_path)
        self.set_chmod(self.destination_path)
        self.meta['torrent_creation_state'] =  self.worker_state
        self.meta['torrent_creation_date'] =  datetime.now()
        self.ingest_dcp_state = WorkerResult.OK
        self.ingest_step = IngestAutoStep.DONE
        self.meta['ingest_dcp_state'] =  self.ingest_dcp_state
        self.meta['ingest_dcp_date'] =  datetime.now()
        self.meta['progress'] = 100.0
        self.isDone = True

    def copy(self, no_copy_files=None) :
        self.ingest_step = IngestAutoStep.COPYING
        q = queue.Queue()
        start = timeit.default_timer()
        self.thread = Copy(q, self.dcp_source_path, self.destination_path, no_copy_files)
        self.thread.start()
        while self.thread.is_alive() :
            self.progress = self.thread.get_progress()
            self.thread.join(1.0)

        thread_duration = timeit.default_timer() - start
        logger.info("Copy DCP Duration = {}".format(thread_duration))

        if not q.empty():
            self.worker_state,  self.worker_msg = q.get()
        return self.worker_state

    def verify(self):
        self.ingest_step = IngestAutoStep.VERIFYING
        tqueue = queue.Queue()
        start = timeit.default_timer()
        if self.copy_dcp:
            self.thread = TdcpbCheckThread(tqueue,
                os.path.join(
                    self.destination_path,
                    self.dcp_name),
                 'long')
        else:
            self.thread = TdcpbCheckThread(tqueue,
                    self.dcp_source_path,
                 'long')

        self.thread.start()
        while True :
            try:
                msg = tqueue.get(block=False)
            except queue.Empty:
                pass
            else:
                if 'workerState' in msg:
                    self.worker_state = msg['workerState']
                if 'exception' in msg:
                    exc_type, exc_value, exc_trace =  msg['exception']
                    raise TdcpbException(exc_value)
            self.thread.join(1)
            step, progress = self.thread.get_progress()
            if self.thread.isAlive():
                continue
            else:
                break
        try:
            msg = tqueue.get(block=False)
        except queue.Empty:
            pass
        else:
            if 'workerState' in msg:
                self.worker_state = msg['workerState']
            if 'exception' in msg:
                exc_type, exc_value, exc_trace =  msg['exception']
                raise TdcpbException(exc_value)


        thread_duration = timeit.default_timer() - start
        logger.info("Verify DCP Duration = {}".format(thread_duration))
        self.verify_duration = thread_duration

        return self.worker_state

    def create_torrent(self):
        self.ingest_step = IngestAutoStep.CREATING_TORRENT
        q = queue.Queue()
        start = timeit.default_timer()

        torrent_file = os.path.join(
            os.path.abspath(self.torrent_files_path),
            self.dcp_name + ".torrent")

        if self.copy_dcp:
            self.thread = TorrentCreatorThread(
                torrent_file,
                os.path.join(
                    self.destination_path,
                    self.dcp_name),
                q)
        else:
            self.thread = TorrentCreatorThread(
                torrent_file,
                self.dcp_source_path,
                q)

        self.thread.duration_estimation = self.verify_duration
        self.thread.start()
        while self.thread.is_alive() :
            self.progress = self.thread.get_progress()
            self.thread.join(1.0)

        thread_duration = timeit.default_timer() - start

        logger.info("Create Torrent Duration = {}".format(thread_duration))

        if not q.empty():
            self.worker_state,  self.worker_msg = q.get()
        return self.worker_state




    def get_progress(self):
        if self.thread :
            if self.ingest_step == IngestAutoStep.COPYING:
                self.progress = (self.thread.get_progress()/3)
            elif self.ingest_step == IngestAutoStep.VERIFYING:
                step, progress = self.thread.get_progress()
                self.progress = (progress/3) + 33.0
            elif self.ingest_step == IngestAutoStep.CREATING_TORRENT:
                self.progress = (self.thread.get_progress()/3) + 66.0
        if self.ingest_step == IngestAutoStep.DONE : self.progress = 100.0
        return (self.ingest_step, self.progress)

    def get_meta(self):
        self.meta['progress'] = self.progress
        self.meta['ingest_step'] = self.ingest_step

        return self.meta

def ingest(p_source, p_dir_torrent, b_copy_dcp=True,  p_destination =None):
   # 1st verify if DCP is present and well formed
    try:
        tdcpb_check_files(p_source)
    except TdcpbException as _err:
        raise TdcpbException(_err)
    # if no error copy it
    _source = os.path.abspath(p_source)
    if b_copy_dcp == True:
        try :
            copy_dcp(p_source, p_destination)
        except TdcpbException as _err:
            raise TdcpbException(_err)
        # once copied verify files on destination
        # ie make sure cp is OK
        _destination = os.path.abspath(p_destination)
        _dcp_name = os.path.basename(_source)
        _dcp_folder_destination = os.path.join(_destination, _dcp_name)
        # verify copy ok
        try:
            tdcpb_check_files(_dcp_folder_destination)
        except TdcpbException as _err:
            raise TdcpbException(_err)
    else:
        if not os.path.isdir(_source):
            _err="{} is not a valid directory".format(_source)
            raise TdcpbException(_err)

        logger.debug("Skipping copy of {}".\
                format(os.path.basename(_source)))
        _dcp_folder_destination = os.path.abspath(_source)
    # verify dcp integrity
    try:
        tdcpb_check_hash(_dcp_folder_destination)
    except TdcpbException as _err:
        raise TdcpbException(_err)

    # create torrent
    if p_dir_torrent is not None:
        logger.debug("Torrent dir: {}".format(p_dir_torrent))
        if not os.path.exists(p_dir_torrent):
            _err = "No valid torrent directory path given"
            raise TdcpbException(_err)
        _dir_torrent = os.path.abspath(p_dir_torrent)
        try:
            tdcpb_make_torrent(_dcp_folder_destination,
                               _dir_torrent)
        except TdcpbException as _err:
            raise TdcpbException(_err)
    logger.info("DCP ingest SUCCESSFULL for {}".\
            format(os.path.basename(_source)))


