#
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4
#
#
# Copyright Nicolas Bertrand (nicolas@indecp.org), 2018
#
# This file is part of tdcpbtools.
#
#    tdcpbtools is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    tdcpbtools is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with tdcpbtools.  If not, see <http://www.gnu.org/licenses/>.
#
#
# Usage:
#  TODO

import sys
import os.path
import threading
import queue
import time
import json
from .common import TdcpbException
from . import logger, WorkerResult, IngestAutoStep

from . import di_parser as T_PARSER

CONTENT_KIND={
'trailer': 'TLR',
'feature': 'FTR',
'episode': 'EPS',
'teaser' : 'TSR',
'promo' : 'PRO',
'test' : 'TST',
'short' : 'SHR',
'advertisement' : 'ADV',
'transitional' : 'XSN',
'psa' : 'PSA',
'policy' : 'POL'
}


def find(key, dictionary):
    for k, v in dictionary.items():
        if k == key:
            yield v
        elif isinstance(v, dict):
            for result in find(key, v):
                yield result
        elif isinstance(v, list):
            for d in v:
                for result in find(key, d):
                    yield result

def tdcpb_dump(p_dcp_folder):
    _dcp_folder = os.path.abspath(p_dcp_folder)
    logger.info('DCP_DUMP {}'\
        .format(os.path.basename(_dcp_folder)))
    # do some basic check
    if not os.path.exists(_dcp_folder):
        _msg = ("tdcpb dump:  Dcp directory {} does not exist"\
            .format(_dcp_folder))
        raise TdcpbException(_msg)
    #TODO : why not use normpath ?
    try :
        DCP = T_PARSER.DiParser(_dcp_folder)
        res = DCP.dump()
    except T_PARSER. DiError as _err:
        raise TdcpbException(_err)
    return res

def tdcpb_contentkind(p_dcp_folder):
    try :
        data = tdcpb_dump(p_dcp_folder)
    except TdcpbException as _err:
        raise TdcpbException(_err)
    contentkind = list(find("ContentKind", data))
    if contentkind is []:
        msg="No ContentKind tag found"
        raise TdcpbException(_err)
    contentkind = contentkind.pop()

    if contentkind in CONTENT_KIND:
        return CONTENT_KIND[contentkind]
    else:
        return 'UKN'

def is_encrypted(data):
    encrypted = list(find("Encrypted", data))
    if encrypted is []:
        msg="No Cencyption tag found"
        raise TdcpbException(_err)
    return encrypted.pop()




def tdcpb_check_short(p_dcp_folder):
    unexpected_files = None
    _dcp_folder = os.path.abspath(p_dcp_folder)
    logger.info('File check started for {}'\
        .format(os.path.basename(_dcp_folder)))
    # do some basic check
    if not os.path.exists(_dcp_folder):
        _msg = " check short:  dcp directory {} does not exist"\
            .format(_dcp_folder)
        raise TdcpbException(_msg)
    #TODO : why not use normpath ?
    try :
        DCP = T_PARSER.DiParser(_dcp_folder)
        _nb = DCP.check_files()
    except T_PARSER. DiError as _err:
        raise TdcpbException(_err)
    if _nb == 0:
        _err = "DCP {} not well formed "\
            .format(os.path.basename(_dcp_folder))
        raise TdcpbException(_err)
    unexpected_files = DCP.unexpected
    if unexpected_files :
        logger.info('Unexpected files: {}'\
            .format(','.join(unexpected_files)))
    logger.info('File check OK for {}'\
        .format(os.path.basename(_dcp_folder)))
    return unexpected_files


class TdcpbCheckThread(threading.Thread):

    def __init__(self, queue, dcp_path, verification_type):
        threading.Thread.__init__(self)
        self.queue = queue
        self.dcp_path = dcp_path
        self.verification_type = verification_type
        self.isCheckvalid = False
        self.workerResult = WorkerResult.ON_PROGRESS
        try :
            self.parser = T_PARSER.DiParser(self.dcp_path)
        except T_PARSER. DiError as _err:
            raise TdcpbException(_err)

    def queue_put(self, *args,**kwargs):
        queue_state= {
            'isCheckvalid': self.isCheckvalid ,
            'WorkerResult': self.workerResult
        }
        for k,v in kwargs.items():
            print (k,"=", v)
            queue_state[k] = v
        self.queue.put(queue_state)


    def run(self):
        """
        """
        try:
            logger.debug("Starting Dcp verification of ")
            if (self.verification_type == u"short"):
                # TODO verify id short check works correctly in threaded mode
                _res = self.parser.path.check_files()
            elif (self.verification_type == u"long"):
                _res = self.parser.check_hash()
                if _res is not "OK":
                    _err = "DCP hash verfication failed"
                    raise TdcpbException(_err)
                else :
                    self.isCheckvalid = True
                    self.workerResult = WorkerResult.OK
                    self.queue_put()
            else:
                _err = "unknown verfication type:{}".format(p_check_type)
                raise TdcpbException(_err)
        except TdcpbException as _err:
            self.isCheckvalid = False
            self.workerResult = WorkerResult.FAIL
            self.queue_put(exception = sys.exc_info())


    def get_progress(self):
        """
        TODO: DO get_progress http://blog.acipo.com/python-threading-progress-indicators/
        """
        return IngestAutoStep.VERIFYING, self.parser.progress

    def get_check_validity(self):
        """
        TODO: DO get_progress http://blog.acipo.com/python-threading-progress-indicators/
        """
        return self.isCheckvalid


def tdcpb_check_long(p_dcp_folder):
    logger.info("Hash Check started for {}"\
        .format(os.path.basename(p_dcp_folder)))
    # do some basic check
    if not os.path.exists(p_dcp_folder):
        _msg = "Check long: dcp directory {} does not exist"\
            .format(p_dcp_folder)
        raise TdcpbException(_msg)

    _dcp_folder = os.path.abspath(p_dcp_folder)

    q = queue.Queue()
    thread = TdcpbCheckThread(_dcp_folder, 'long', q)
    thread.start()
    while thread.is_alive() :
        progress = thread.get_progress()
        print (" main progress: {}".format(progress))
        time.sleep(1)



def tdcpb_check(p_dcp_folder, p_check_type=u"short"):

    if (p_check_type == u"short"):
        tdcpb_check_short(p_dcp_folder)

    elif (p_check_type == u"long"):
        tdcpb_check_long(p_dcp_folder)
    else:
        _err = "unknow verfication type:{}".format(p_check_type)
        logger.error(_err)
        raise TdcpbException(_err)


