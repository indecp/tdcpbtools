#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -*- Mode: Python -*-
# vim:si:ai:et:sw=4:sts=4:ts=4
#
#
# Copyright Nicolas Bertrand (nicolas@indecp.org), 2019
#
# This file is part of tdcpbtools.
#
#    tdcpbtools is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Luciole is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Luciole.  If not, see <http://www.gnu.org/licenses/>.
#
#
# Usage:
# tdcpb-disk-usage /path/to/folder
#
# Return the disk usage data from the partion where /path/to/folder is present

import sys
import logging
import argparse
import json

from tdcpblib import logger
from tdcpblib.common import TdcpbException
from tdcpblib.disk_usage import disk_usage


def main(argv):
    parser = argparse.ArgumentParser(
        description='Retrieve diskusage information')
    parser.add_argument('directory',
                metavar='DIRECTORY',
                type = str,
                nargs = "?",
                help = 'DCP path' )
    parser.add_argument('-d', '--debug', dest='debug', action='store_const',
                   const=logging.DEBUG, default=logging.INFO,
                   help='debug mode')

    args = parser.parse_args()
    logger.setLevel(args.debug)

    if not args.directory:
        logger.error("No  source")
        return 1
    try:
        data = disk_usage(args.directory)
        print ("disk_usage:", data)
        #print( json.dumps(data, sort_keys=True, indent=2))
    except TdcpbException as _err:
        logger.error(_err)
        return 1
    return 0

if __name__ == "__main__":
   sys.exit(main(sys.argv))

