#!/bin/bash
TDCPBTOOLS="/home/nicolas/dev/indecp/tdcpbtools"
DCP_TEST=TheThirdMurder_TLR-Date_F_JA-fr_51_2K_PACTE_20180309_TST_IOP_OV
SOURCE_PATH=/home/nicolas/data/DCP_IN
DESTINATION_PATH=/home/nicolas/data/DCP
TORRENT_DIR=/tmp/torrents



# prepare
DCP_DST="${DESTINATION_PATH}/${DCP_TEST}"
if [ -d "${DCP_DST}" ]; then
	echo "remove $DCP_DST"
	rm -rf ${DCP_DST}
fi

DCP_SRC="${SOURCE_PATH}/${DCP_TEST}"
if [ -d "${DCP_SRC}" ]; then
	echo "$DCP_SRC exists"
	#create an exta dummy file
	dd if=/dev/urandom of="${DCP_SRC}/dummy.txt" bs=2048 count=10 1>/dev/null 2>&1
	mkdir -p ${DCP_SRC}/Foo
	dd if=/dev/urandom of="${DCP_SRC}/Foo/dummy.txt" bs=2048 count=10 1>/dev/null 2>&1
	dd if=/dev/urandom of="${DCP_SRC}/Foo/dummy2.txt" bs=2048 count=10 1>/dev/null 2>&1
	mkdir -p ${DCP_SRC}/.Trash
	dd if=/dev/urandom of="${DCP_SRC}/.dummy.txt" bs=2048 count=10 1>/dev/null 2>&1
	dd if=/dev/urandom of="${DCP_SRC}/.Trash/.dummy.txt" bs=2048 count=10 1>/dev/null 2>&1

fi

if [ ! -d "${TORRENT_DIR}" ]; then
	echo "create ${TORRENT_DIR}"
	mkdir -p ${TORRENT_DIR}
fi


export PYTHONPATH="${TDCPBTOOLS}:$PYTHONPATH"
PYTHON="${TDCPBTOOLS}/venv/bin/python3"
INGEST_SCRIPT="${TDCPBTOOLS}/bin/tdcpb_ingest"
CMD="${PYTHON} ${INGEST_SCRIPT} --torrent ${TORRENT_DIR} ${DCP_SRC}  ${DESTINATION_PATH}"
echo "execute $CMD"
$CMD

ls -trla ${DESTINATION_PATH}/${DCP_TEST}
